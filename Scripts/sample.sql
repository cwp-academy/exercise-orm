:on error exit

if exists (select name from [master].[sys].[databases] where name = N'tigerdb')
	drop database [tigerdb];

if @@ERROR = 3702 
    raiserror('[tigerdb] database cannot be dropped because there are still other open connections', 127, 127) with nowait, log;
go

create database tigerdb;
go

use [tigerdb]
go

create table clients (
	id uniqueidentifier primary key,
	name nvarchar(64) not null,
	short_name nvarchar(64) not null
);
go

create table engagements (
	id uniqueidentifier primary key,
	name nvarchar(64) not null,
	client_id uniqueidentifier not null
);
go

create table engagement_offices (
	id uniqueidentifier primary key,
	office_id uniqueidentifier not null,
	engagement_id uniqueidentifier not null,
	type int not null
);
go

alter table engagement_offices
add constraint ak_engagement_id_office_id unique (engagement_id, office_id);
go

create index idx_engagement_id on engagement_offices (engagement_id);
go

create table offices (
	id uniqueidentifier primary key,
	name nvarchar(64) not null
);
go

create table staffs (
	id uniqueidentifier primary key,
	first_name nvarchar(64) not null,
	last_name nvarchar(64) not null,
	office_id uniqueidentifier not null
);
go

create table engagement_office_contacts (
	id uniqueidentifier primary key,
	engagement_office_id uniqueidentifier not null,
	staff_id uniqueidentifier not null
);
go

create index idx_engagement_office_id on engagement_office_contacts (engagement_office_id);
go

truncate table clients
truncate table offices

insert into clients values ('A9D57B78-0A95-4BD0-B804-0516BD1B2448', 'Google', 'GL');
insert into clients values ('B64C8D20-4B85-40F0-AE4F-216CE28BA75E', 'Microsoft', 'MS');
insert into clients values ('E804DB45-89D7-43E5-A558-2D7B2FE48642', 'Hasting', 'HAS');
insert into clients values ('3AA638A1-DE96-47CA-9248-827C64C3C505', 'International Business Machine', 'IBM');

insert into offices values ('70B09948-BD25-4D07-A60F-3B8F4A459112', 'Beijing');
insert into offices values ('560B39FF-D56A-48C5-9CF2-49A4C433BCDF', 'Singapore');
insert into offices values ('FC42E35F-7FF5-41FD-8838-920858811631', 'London');
insert into offices values ('4B2E3786-5E56-4695-A655-CA6D202F04C6', 'Shanghai');

insert into staffs values (newid(), 'Yingpei', 'Wu', '560B39FF-D56A-48C5-9CF2-49A4C433BCDF');
insert into staffs values (newid(), 'Kangpei', 'Wu', '560B39FF-D56A-48C5-9CF2-49A4C433BCDF');
insert into staffs values (newid(), 'Yingxiong', 'Wu', '560B39FF-D56A-48C5-9CF2-49A4C433BCDF');
insert into staffs values (newid(), 'Yingyong', 'Wu', '560B39FF-D56A-48C5-9CF2-49A4C433BCDF');

insert into staffs values (newid(), 'Xia', 'Liu', '70B09948-BD25-4D07-A60F-3B8F4A459112');
insert into staffs values (newid(), 'Chen', 'Zhou', '70B09948-BD25-4D07-A60F-3B8F4A459112');
insert into staffs values (newid(), 'Xiaowei', 'Liu', '70B09948-BD25-4D07-A60F-3B8F4A459112');
insert into staffs values (newid(), 'Xue', 'Wang', '70B09948-BD25-4D07-A60F-3B8F4A459112');
insert into staffs values (newid(), 'Pei', 'Yan', '70B09948-BD25-4D07-A60F-3B8F4A459112');
insert into staffs values (newid(), 'Xueying', 'Li', '70B09948-BD25-4D07-A60F-3B8F4A459112');

insert into staffs values (newid(), 'James', 'Wang', 'FC42E35F-7FF5-41FD-8838-920858811631');
insert into staffs values (newid(), 'James', 'Zhang', 'FC42E35F-7FF5-41FD-8838-920858811631');
insert into staffs values (newid(), 'James', 'Li', 'FC42E35F-7FF5-41FD-8838-920858811631');
insert into staffs values (newid(), 'James', 'Zhao', 'FC42E35F-7FF5-41FD-8838-920858811631');
insert into staffs values (newid(), 'James', 'Huang', 'FC42E35F-7FF5-41FD-8838-920858811631');

insert into staffs values (newid(), 'Jiessy', 'Xia', '4B2E3786-5E56-4695-A655-CA6D202F04C6');
insert into staffs values (newid(), 'Emma', 'Wu', '4B2E3786-5E56-4695-A655-CA6D202F04C6');
insert into staffs values (newid(), 'Richard', 'Wu', '4B2E3786-5E56-4695-A655-CA6D202F04C6');
go