﻿using System;
using System.Collections.Generic;
using System.Reflection;
using FluentNHibernate.Cfg;
using FluentNHibernate.Cfg.Db;
using NHibernate;
using Xunit.Abstractions;

namespace Exercise.Orm
{
    class DatabaseContext : IDisposable
    {
        readonly ISessionFactory sessionFactory;
        readonly List<ISession> sessions = new List<ISession>();
        readonly OutputRedirector outputRedirector;

        static string GetConnectionString()
        {
            return @"Data Source=.\SQLExpress; Initial Catalog=AdventureWorks2008; Integrated Security=true;";
        }

        public DatabaseContext(ITestOutputHelper outputHelper)
        {
            sessionFactory = BuildSessionFactory();
            outputRedirector = new OutputRedirector(outputHelper);
        }

        static ISessionFactory BuildSessionFactory()
        {
            MsSqlConfiguration databaseConfig = MsSqlConfiguration.MsSql2008
                .ConnectionString(GetConnectionString())
                .ShowSql()
                .FormatSql();
            return Fluently
                .Configure()
                .Database(databaseConfig)
                .Mappings(m => m.FluentMappings.AddFromAssembly(Assembly.GetExecutingAssembly()))
                .BuildSessionFactory();
        }

        public ISession CreateSession()
        {
            ISession session = sessionFactory.OpenSession();
            sessions.Add(session);
            return session;
        }

        public void Dispose()
        {
            sessions.ForEach(s => s.Dispose());
            sessionFactory.Dispose();
            outputRedirector.Dispose();
        }
    }
}