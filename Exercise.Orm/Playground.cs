﻿using System;
using NHibernate;
using Xunit.Abstractions;

namespace Exercise.Orm
{
    public abstract class Playground : IDisposable
    {
        readonly DatabaseContext databaseContext;
        protected readonly ITestOutputHelper outputHelper;

        protected Playground(ITestOutputHelper outputHelper)
        {
            databaseContext = new DatabaseContext(outputHelper);
            this.outputHelper = outputHelper;
        }

        protected ISession CreateSession()
        {
            return databaseContext.CreateSession();
        }

        public void Dispose()
        {
            databaseContext.Dispose();
        }
    }
}
