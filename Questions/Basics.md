## 1

How to map a table to an simple object? Take `Client` and *[clients]* table as an example.

## 2

How to map multiple columns to a single property. Take `Staff` and *[staffs]* as an example.

```csharp
class Staff {
    public virtual Guid Id { get; set; }
    public virtual Name Name { get; set; }
    public virtual Guid OfficeId { get; set; }
}

class Name {
    public string First { get; set; }
    public string Last { get; set; }
}
```

## 3

How to map a one-to-many relationship? Take `Engagement` and `EngagedOffice` as an example.

## 4

Same as question 3. I want to eager load `Engagement` and its `EngagedOffice` collections. How to do that?

## 5

Same as question 3. This time I want to lazy load `Engagement` and its `EngagedOffice` collections. How to do that? When to use lazy load and when to use eager load.

## 6

Given an existed object (may be queried from the database), can I call `Save()` and pass that object as argument?

## 7

How to update an exist object? How to update a one-to-many relationship on an object? (Add/Delete/Update)? Take `Engagement` and `EngagedOffice` as an example.

## 8

How to map many-to-one relationship. Take `EngagedOffice` and `Engagement` as an example.

## 9

How to batch insert multiple objects without executing sql one per each time `Save()` was called.